<?php

namespace Wizinteractive\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{

    protected $name = 'media';

    protected $columns = [];

    protected $fillable = [
        'path',
        'mime_type',
        'slot'
    ];

    protected $rules = [];

    protected $appends = [];

    public function mediable()
    {
        return $this->morphTo();
    }
}
