<?php

namespace Wizinteractive\Cms\Models\Traits;

trait Mediable
{
    public function media()
    {
        return $this->morphMany('Wizinteractive\Cms\Models\Media', 'mediable');
    }
    
    
    public function first($slot)
    {
        $media = $this->media()->where('slot', $slot)->first();
        return $media;
        
    }

    public function getMediaFromSlot($slot)
    {
        return $this->media()->where('slot', $slot)->get();
    }
    
     public function getFirstMediaFromSlot($slot)
    {
        $media = $this->media()->where('slot', $slot)->first();
        return $media;
        
    }
    
}
