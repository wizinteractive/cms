<?php

namespace Wizinteractive\Cms\Models\Traits;

trait Sluggable
{
    public static function bootSluggable()
    {
        self::saving(function ($model) {
            $slugColumn = $model->getSlugColumn();
            if (is_null($model->slug) || strpos($model->slug, str_slug($model->$slugColumn)) === false) {
                $model->slug = str_slug($model->$slugColumn);

                $modelClass = get_class($model);
                $slugExists = !is_null($modelClass::where('slug', $model->slug)->first()) && $modelClass::where('slug', $model->slug)->first()->id !== $model->id;

                while ($slugExists) {
                    $model->slug .= '_' . rand(0, 9);
                    $slugExists = !is_null($modelClass::where('slug', $model->slug)->first());
                }
            }
        });
    }

    public function getSlugColumn()
    {
        if ($this->slugColumn) {
            return $this->slugColumn;
        }

        return 'title';
    }
}
