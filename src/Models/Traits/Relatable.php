<?php

namespace Wizinteractive\Cms\Models\Traits;

trait Relatable
{
    /**
     * Checks if a the model has direct relationships
     */
    public function hasDirectRelations()
    {
        return !empty($this->directRelations);
    }

    /**
     * This will resolve the model relations. If any exist their rules and fillable fields will be added to the model
     */
    public function resolveDirectRelations()
    {
        if ($this->directRelations) {
            $fillable = [];
            $rules = [];
            foreach ($this->directRelations as $resourceName => $resourceInfo) {
                if (is_array($resourceInfo)) {
                    array_push($fillable, $this->getDirectRelationColumn($resourceName));

                    if (array_key_exists('rule', $resourceInfo)) {
                        $rules[$this->getDirectRelationColumn($resourceName)] = $resourceInfo['rule'];
                    }
                    continue;
                }
                array_push($fillable, $resourceInfo);
            }
            $this->addFillable($fillable);
            $this->addRules($rules);
        }
    }

    /**
     * Gets the array where the direct relations are defined
     */
    public function getDirectRelations()
    {
        return $this->directRelations;
    }

    /**
     * Given the relation resource name it returns the id (foreign key) that points to the relation resource
     */
    public function getDirectRelationId($resource)
    {
        if (empty($this->directRelations || !array_key_exists($resource, $this->directRelations))) {
            return null;
        }

        if (is_array($this->directRelations[$resource])) {
            $relation =  array_key_exists('column', $this->directRelations[$resource]) ? $this->directRelations[$resource]['column'] : null;

            return is_null($relation) ? null : $this->$relation;
        }

        $relation = $this->directRelations[$resource];

        return $this->$relation;
    }

    /**
     * Gets the column name of the relation foreign key
     */
    public function getDirectRelationColumn($resource)
    {
        if (empty($this->directRelations || !array_key_exists($resource, $this->directRelations))) {
            return null;
        }

        if (is_array($this->directRelations[$resource])) {
            return array_key_exists('column', $this->directRelations[$resource]) ? $this->directRelations[$resource]['column'] : str_replace('-', '_', snake_case($resource));
        }

        return $this->directRelations[$resource];
    }

    /**
     * Gets the relation resource name
     */
    public function getDirectRelationLabel($resource)
    {
        if (empty($this->directRelations || !array_key_exists($resource, $this->directRelations))) {
            return null;
        }

        return trans("resources.{$resource}.resource-name");
    }

    // indirect relations

    /**
     * Checks if a the model has indirect relationships
     */
    public function hasIndirectRelations()
    {
        return !empty($this->indirectRelations);
    }

    public function getIndirectRelations()
    {
        return $this->indirectRelations;
    }

    public function getIndirectRelation($resource)
    {
        if (empty($this->indirectRelations || !array_key_exists($resource, $this->indirectRelations))) {
            return null;
        }

        if (is_array($this->indirectRelations[$resource])) {
            return array_key_exists('relation', $this->indirectRelations[$resource]) ? $this->indirectRelations[$resource]['relation'] : str_replace('-', '_', snake_case($resource));
        }

        return $this->indirectRelations[$resource];
    }

    /**
     * Gets the relation resource name
     */
    public function getIndirectRelationLabel($resource)
    {
        if (empty($this->indirectRelations || !array_key_exists($resource, $this->indirectRelations))) {
            return null;
        }

        return trans("resources.{$resource}.resource-name");
    }

    public function getIndirectlyRelated($resource)
    {
        if (empty($this->indirectRelations || !array_key_exists($resource, $this->indirectRelations))) {
            return null;
        }

        if (is_array($this->indirectRelations[$resource]) &&
            array_key_exists('relation', $this->indirectRelations[$resource])) {
            $relation = $this->indirectRelations[$resource]['relation'];
            return $this->$relation->pluck('id')->toArray();
        }

        $relation = $this->indirectRelations[$resource];

        return $this->$relation->pluck('id')->toArray();
    }
}
