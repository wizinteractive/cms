<?php

namespace Wizinteractive\Cms\Models\Traits;

use LaravelLocalization;

trait Translatable
{
    public static function bootTranslatable()
    {
        self::saved(function ($model) {
            if ($model->isTranslatable()) {
                $model->updateTranslations();
            }
        });
    }

    public function translations()
    {
        return $this->hasMany(get_class($this), 'translates_id');
    }

    /**
     * Gets the entry that originated the translation
     */
    public function origin()
    {
        return $this->belongsTo(get_class($this), 'translates_id');
    }

    /**
     * Returns the route path to translate a resource
     */
    public function getTranslationRoute()
    {
        return route("admin.{$this->resource}.translate", ['id' => $this->id, 'lang' => $this->lang]);
    }

    /**
     * Returns the current applications available languages (locales)
     */
    public function getLanguages()
    {
        return LaravelLocalization::getSupportedLocales();
    }

    // Mutators

    public function getTranslationRouteAttribute()
    {
        return $this->translatable ? $this->getTranslationRoute() : null;
    }

    public function getSupportedTranslationRoutesAttribute()
    {
        if ($this->isTranslatable()) {
            $languages = [];

            foreach ($this->getLanguages() as $code => $language) {
                if ($code !== \App::getLocale()) {
                    $languages[$code] = $language;
                    $languages[$code]['route'] = route("admin.{$this->resource}.translate", ['id' => $this->id, 'lang' => $code]);
                }
            }
            return $languages;
        }
        return null;
    }

    /**
     * Gets the translatable columns of a given admin resource
     *
     * @param  void
     * @return array
     */
    public function getTranslatableColumns()
    {
        $translatable = [];

        foreach ($this->columns as $column => $type) {
            if (in_array($column, $this->translatableColumns)) {
                $translatable[$column] = $type;
            }
        }
        return $translatable;
    }

    /**
     * Ensures that are resource translations are up to date
     *
     *
     * @return void
     */
    public function updateTranslations()
    {
        $nonTranslatableFields = array_diff($this->getColumns(), $this->getTranslatableColumns());
        $updatedFields = [];
        foreach ($nonTranslatableFields as $field => $type) {
            $updatedFields[$field] = $this->$field;
        }
        foreach ($this->translations as $translation) {
            $translation->update($updatedFields);
        }
    }
}
