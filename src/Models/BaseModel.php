<?php

namespace Wizinteractive\Cms\Models;

use Wizinteractive\Cms\Models\Traits\Mediable;
use Wizinteractive\Cms\Models\Traits\Relatable;
use Wizinteractive\Cms\Models\Traits\Translatable;
use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{

    use Relatable, Mediable, Translatable;

    protected $resource = '';

    protected $designationField = 'title';

    /**
    *
    * Defines the models to which this relates directly (there will be a foreign key to these
    * models on this model's table)
    */
    protected $directRelations = [];

    protected $columns = [
        'title' => 'string'
    ];

    protected $display = [
        'title'
    ];

    protected $fillable = [
        'title'
    ];

    protected $rules = [
        'title' => 'required|max:255'
    ];

    protected $actions = [
        'create' => 'create',
        'read' => 'show',
        'update' => 'edit',
        'delete' => 'destroy'
    ];

    protected $appends = [
        //'columns',
        'is_mediable',
        'designation',
        //'action_routes',
        'display_type',
        'translation_route',
        'supported_translation_routes'
    ];

    protected $with = ['media'];

    protected $mediable = true;

    protected $translatable = false;

    protected $translatableColumns = [];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->resolveDirectRelations();
    }

    /**
     * defined the media slots associated with a model
     *
     * @return array
     */
    public function mediaSlots()
    {
        return [
            [
                'name' => 'image',
                'label' => 'Imagem',
                'multiple' => false,
                'validation' => '' //TO DO
            ]
        ];
    }

    /**
     * adds entries to the $actions array
     *
     * @param  array
     * @return void
     */
    public function addActions($actions = [])
    {
        $this->actions = array_merge($this->actions, $actions);
    }

    /**
     * adds entries to the $appends array
     *
     * @param  array
     * @return void
     */
    public function addAppends($appends = [])
    {
        $this->appends = array_merge($this->appends, $appends);
    }

    /**
     * adds entries to the $fillable array
     *
     * @param  array
     * @return void
     */
    public function addFillable($fillable = [])
    {
        $this->fillable = array_merge($this->fillable, $fillable);
    }

    /**
     * adds entries to the $rules array
     *
     * @param  array
     * @return void
     */
    public function addRules($rules = [])
    {
        $this->rules = array_merge($this->rules, $rules);
    }

    public static function boot()
    {
        parent::boot();

        self::saving(function ($model) {
            if (!isset($model->lang) && $model->isTranslatable()) {
                $model->lang = app()->getLocale();
            }
        });
    }

    /**
     * Gets the class of a given admin resource
     *
     * @param  void
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * Gets the class of a given admin resource
     *
     * @param  array
     * @return void
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    /**
     * Gets the columns that should be displayed when listing a model
     *
     * @param  void
     * @return array
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * Gets the rules the resource's validation
     *
     * @param  void
     * @return array
     */
    public function getRules()
    {
        return $this->rules;
    }

    /**
     * Gets the label for a given column
     *
     * @param  string $column
     * @return string $label
     */
    public function getColumnLabel($column)
    {
        if ($this->resource === '') {
            return trans("resources.${str_singular(get_class())}.{$column}");
        }
        return trans("resources.{$this->resource}.{$column}");
    }

    /**
     * Gets the value for a given column
     *
     * @param  string $column
     * @return string $label
     */
    public function getColumnValue($column)
    {
        return $this->$column;
    }

    /**
     * Gets the field options if there is any
     *
     * @param  string $column
     * @return array $options
     */
    public function getFieldOptions($column)
    {
        $field = snake_case($column);
        $method = camel_case("get_field_{$field}_options");

        if (method_exists($this, $method)) {
            return $this->$method();
        }

        return [];
    }

    public function isMediable()
    {
        return $this->mediable;
    }

    public function isTranslatable()
    {
        return $this->translatable;
    }

    public function getActions()
    {
        return $this->actions;
    }

    //scopes
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }

    //mutators
    public function getColumnsAttribute()
    {
        return collect($this->columns)->reduce(function ($reduced, $column) {
            $reduced[$column] = $this->getColumnLabel($column);
            return $reduced;
        }, []);
    }

    public function getIsMediableAttribute()
    {
        return $this->mediable;
    }

    public function getDesignationAttribute()
    {
        $field = $this->designationField;

        if (is_null($this->$field)) {
            return $this->resource;
        }

        return $this->$field;
    }

    public function getDisplayTypeAttribute()
    {
        return trans("resources.{$this->resource}.resource-name");
    }

    public function getActionRoutesAttribute()
    {
        $resource = $this->resource;
        $routes = [];

        foreach ($this->actions as $action => $label) {
            $routes[$label] = route("admin.{$resource}.{$label}", ['id' => $this->id]);
        }
        return $routes;
    }
}
