<?php

return [
    'redirect' => [
        'after-login' => 'admin/dashboard',
        'after-logout' => 'admin/login',
        'after-register' => 'admin/login',
    ],
    'enable_register_link' =>false, //enables or disabes links to allow user register on login
];
