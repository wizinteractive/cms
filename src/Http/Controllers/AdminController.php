<?php

namespace Wizinteractive\Cms\Http\Controllers;

use Wizinteractive\Cms\Http\Controllers\Controller;
use Wizinteractive\Cms\Support\AdminResourceManager;
use Wizinteractive\Cms\Models\BaseModel;
use Wizinteractive\Cms\Http\Controllers\Traits\CanHandleMedia;
use Illuminate\Http\Request;
use Validator;
use LaravelLocalization;

class AdminController extends Controller
{
    use CanHandleMedia;

    /**
     * Support class that implements an AdminServiceContract and handles most of the logic of the admin controller
     */
    protected $manager;

    /**
     * Page to which the user is redirect after loging in as admin
     */
    protected $redirectTo = '/admin';

    /**
     * Auth guard specifies a 'context' where a user is authenticated
     */
    protected $guard = 'admin';

    /**
     * Resource being handled by the controller
     */
    protected $resource = null;

    public function __construct(AdminResourceManager $manager)
    {
        parent::__construct();
        $this->middleware('admin');
        $this->manager = $manager;
        /**
         * Resolves the current resource based on the current admin route
         */
        $this->resource = $manager->resolveResource(request()->path());
    }

    /**
     * Updates the list of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function updateList()
    {
        try {
            throw new \Exception("This should be implemented in the application context. Check documentation for further information.", 1);
        } catch (\Exception $e) {
            return redirect()
                    ->route("admin.{$this->resource}.index")
                    ->withErrors([
                        $e->getMessage()
                    ]);
        }
        return redirect()
                ->route("admin.{$this->resource}.index")
                ->with('response', [
                    'status' => 0,
                    'message' => 'Resource updated!'
                ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /**
         * If the controller fails to resolve the current resource the flow stops
         * and an 'unresolved-resource' error view is displayed
         */
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        /**
         * If the controller fails to resolve the current resource the flow stops
         * and an 'unresolved-resource' error view is displayed
         */
        $className = $this->manager->getResourceClass($this->resource);

        if (is_null($className)) {
            return view('admin.errors.inexistent-model');
        }

        $query = $className::query();

        // check if the resource is translatable and return the query accordingly
        if ($this->manager->instantiateResource($this->resource)->isTranslatable()) {
            $query = $query->where('lang', app()->getLocale())->where('translates_id', null);
        }

        // checks for query strings in the url and tries to append them to the items' query
        $queryStrings = array_except(request()->query(), 'page');

        if (!empty($queryStrings)) {
            foreach ($queryStrings as $column => $value) {
                $query = $query->where($column, $value);
            }
        }

        // paginates the item list with 15 per page items and appends any query strings
        $items = $query->paginate(1000)->appends($queryStrings);

        return view($this->manager->resolveResourceView($this->resource, 'index'), [
            'resource' => $this->resource,
            'items' => $items,
            'token' => csrf_token(),
            'actions' => $this->manager->getResourceActions($this->resource)
        ]);

        return view('admin.errors.inexistent-model');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }


        /**
         * Gets the current resource class and instantiates a the model object from it
         */

        $className = $this->manager->getResourceClass($this->resource);

        $model = $this->manager->instantiateResource($this->resource);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        /**
         * Gets all the direct relations of the current model
         */

        $directRelationsLists = $this->manager->getResourceDirectRelationsLists($this->resource);
        $indirectRelationsLists = $this->manager->getResourceIndirectRelationsLists($this->resource);

        return view($this->manager->resolveResourceView($this->resource, 'edit'), [
            'resource' => $this->resource,
            'model' => $model,
            'directRelationsLists' => $directRelationsLists,
            'indirectRelationsLists' => $indirectRelationsLists,
            'panel' => 'store',
            'token' => csrf_token(),
            'actions' => $this->manager->getResourceActions($this->resource)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $model = $this->manager->instantiateResource($this->resource);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        $this->validate($request, $model->getRules());

        $files = $this->prepareMedia($request, $model);

        // extracts all the request fields excepting the csrf token
        $fields = $this->prepareFields($request, $model);

        $model->fill($fields)->save();
        $this->handleIndirectRelations($request, $model);
        $model->media()->saveMany($files);

        return redirect()
                ->route("admin.{$this->resource}.edit", [
                    str_singular($this->resource) => $model->id
                ])
                ->with('response', [
                    'status' => 0,
                    'message' => 'Resource saved!'
                ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $className = $this->manager->getResourceClass($this->resource);

        $model = $className::find($id);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        return view($this->manager->resolveResourceView($this->resource, 'show'), [
            'resource' => $this->resource,
            'model' => $model,
            'token' => csrf_token()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $className = $this->manager->getResourceClass($this->resource);

        $model = $className::find($id);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        $directRelationsLists = $this->manager->getResourceDirectRelationsLists($this->resource);
        $indirectRelationsLists = $this->manager->getResourceIndirectRelationsLists($this->resource);
        //dd($indirectRelationsLists, 'ie');
        //dd($directRelationsLists);

        return view($this->manager->resolveResourceView($this->resource, 'edit'), [
            'resource' => $this->resource,
            'directRelationsLists' => $directRelationsLists,
            'indirectRelationsLists' => $indirectRelationsLists,
            'model' => $model,
            'panel' => 'update',
            'token' => csrf_token(),
            'actions' => $this->manager->getResourceActions($this->resource)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $className = $this->manager->getResourceClass($this->resource);

        $model = $className::find($id);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        $this->validate($request, $model->getRules());

        $files = $this->prepareMedia($request, $model);

        // extracts all the request fields excepting the csrf token
        $fields = $this->prepareFields($request, $model);
        $model->fill($fields)->save();
        $this->handleIndirectRelations($request, $model);

        if (request('media_to_remove')) {
            $this->removeMedia(request('media_to_remove'));
        }
        $model->media()->saveMany($files);

        return redirect()
                ->route("admin.{$this->resource}.edit", [
                    str_singular($this->resource) => $model->id
                ])
                ->with('response', [
                    'status' => 0,
                    'message' => 'Resource updated!'
                ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $className = $this->manager->getResourceClass($this->resource);

        $model = $className::find($id);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        $model->delete();
        $model->media()->delete();

        return redirect()
                ->route("admin.{$this->resource}.index")
                ->with('response', [
                    'status' => 0,
                    'message' => 'Resource deleted!'
                ]);
    }

    /**
     * Translate the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function translate($id, $lang)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $className = $this->manager->getResourceClass($this->resource);


        $model = $className::where('translates_id', $id)
                                    ->where('lang', $lang)
                                    ->first();

        if (is_null($model)) {
            $model = $className::find($id);
        }

        return view($this->manager->resolveResourceView($this->resource, 'translate'), [
            'resource' => $this->resource,
            'model' => $model,
            'lang' => $lang,
            'token' => csrf_token(),
            'actions' => $this->manager->getResourceActions($this->resource)
        ]);
    }

    public function postTranslate($id, $lang)
    {
        if (is_null($this->resource)) {
            return view('admin.errors.unresolved-resource');
        }

        $className = $this->manager->getResourceClass($this->resource);

        $model = $className::find($id);

        if (is_null($model)) {
            return view('admin.errors.inexistent-model');
        }

        $translation = $className::where('translates_id', $model->id)
                                    ->where('lang', $lang)
                                    ->first();

        if (is_null($translation)) {
            $translation = $model->replicate();
        }

        $fields = $this->prepareFields(request(), $translation, true);

        $translation->lang = $lang;
        $translation->translates_id = $model->id;
        $translation->save();
        $translation->update($fields);

        return redirect()
                ->route("admin.{$this->resource}.edit", [
                    str_singular($this->resource) => $model->id
                ])
                ->with('response', [
                    'status' => 0,
                    'message' => 'Resource Translated!'
                ]);
    }

    /**
     * Prepare fields to save
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  Wizinteractive\Cms\Models\BaseModel  $Model
     * @param  boolean $tranlation exclude fields that are not translatable from array
     * @return array $fields
     */
    public function prepareFields(Request $request, BaseModel $model, $translation = false)
    {
        $fields = array_except($request->all(), ['_token', 'files', '_method']);

        if (!$translation) {
            foreach ($model->getColumns() as $column => $type) {
                if (!array_key_exists($column, $fields)) {
                    $fields[$column] = 0;
                }
            }
        }
        return $fields;
    }

    public function handleIndirectRelations(Request $request, BaseModel $model)
    {
        if ($model->hasIndirectRelations()) {
            $relations = $model->getIndirectRelations();

            foreach ($relations as $resource => $relation) {
                if (is_array($relation) && array_key_exists('relation', $relation)) {
                    if ($model->exists) {
                        $model->$relation['relation']()->sync(request($relation['relation']));
                        continue;
                    }
                    $model->$relation['relation']()->sync(request($relation['relation']));
                    continue;
                }

                if ($model->exists) {
                    $model->$relation()->sync(request($relation));
                    continue;
                }
                $model->$relation()->sync(request($relation));
            }
        }
    }
}
