<?php

namespace Wizinteractive\Cms\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use View;
use Wizinteractive\Cms\Support\Views\ViewResolver;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $viewResolver;

    public function __construct()
    {
        $this->viewResolver = new ViewResolver;
    	/**
	     * An instance of the class ViewResolver is shared across all the views. This class will resolve the package default views unless the user has overriden them in the app context
	     */
        View::share('view', $this->viewResolver);
    }
}
