<?php

namespace Wizinteractive\Cms\Http\Controllers\Traits;

use Illuminate\Http\Request;
use Wizinteractive\Cms\Models\Media;
use Wizinteractive\Cms\Models\BaseModel;

trait CanHandleMedia
{
    /**
     * Prepare media to save
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array $files
     */
    public function prepareMedia(Request $request, $model, $name = 'files', $storagePath = 'media/images', $disk = 'public')
    {
        $files = [];

        foreach ($model->mediaSlots() as $slot) {
            if ($request->hasFile($slot['name'])) {
                foreach ($request->file($slot['name']) as $file) {
                    $mimeType = $file->getMimeType();
                    $path = $file->store($storagePath, $disk);

                    if (!$path) {
                        throw new \Exception('Couldn\'t store the file');
                    }

                    array_push($files, new Media([
                        'slot' => $slot['name'],
                        'path' => $path,
                        'mime_type' => $mimeType
                    ]));
                }
            }
        }
        return $files;
    }

    public function removeMedia($mediaToRemove)
    {
        try {
            foreach ($mediaToRemove as $media) {
                if ($media) {
                    $toRemove = Media::find($media);

                    if ($toRemove) {
                        unlink(public_path($toRemove->path));
                        $toRemove->delete();
                    }
                }
            }
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}
