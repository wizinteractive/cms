<?php

namespace Wizinteractive\Cms\Http\Controllers;

use Wizinteractive\Cms\Http\Controllers\Controller;
use Wizinteractive\Cms\Support\Views\ViewResolver;
use LaravelLocalization;

class DashboardController extends Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->middleware('admin');
    }

    public function dashboard()
    {
        return view($this->viewResolver->resolve('admin.dashboard'));
    }
}
