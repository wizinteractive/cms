<?php

namespace Wizinteractive\Cms\Http\Controllers\Auth;

use Wizinteractive\Cms\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = 'admin/dashboard';
    protected $redirectTo;
    //protected $redirectAfterLogout = 'admin/login';
    protected $redirectAfterLogout;

    protected $guard = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest')->except('logout');
        $this->redirectTo = config('cms.redirect.after-login');
        $this->redirectAfterLogout = config('cms.redirect.after-logout');
    }

    public function showLoginForm()
    {
        return view($this->viewResolver->resolve('admin.auth.login'));
    }

    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect($this->redirectAfterLogout);
    }

    protected function guard()
    {
        return Auth::guard($this->guard);
    }
}
