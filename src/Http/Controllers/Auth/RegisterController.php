<?php

namespace Wizinteractive\Cms\Http\Controllers\Auth;

use Wizinteractive\Cms\Models\AdminUser;
use Wizinteractive\Cms\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->middleware('guest');
        $this->redirectTo = config('cms.redirect.after-register');
    }

    public function showRegistrationForm()
    {
        if (config('cms.enable_register_link') == true) {
            return view($this->viewResolver->resolve('admin.auth.register'));
        }
        abort(404);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:admin_users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
    	if (config('cms.enable_register_link') == true) {
            return $user = AdminUser::create([
           		'name' => $data['name'],
            	'email' => $data['email'],
            	'password' => bcrypt($data['password']),
        	]);
        }
        abort(404);
        
    }
}
