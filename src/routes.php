<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::middleware(['web', 'admin'])
        ->prefix('admin')
        ->namespace('Wizinteractive\\Cms\\Http\\Controllers')
        ->group(function () {

            Route::post('logout', [
                'as' => 'admin.logout',
                'uses' => 'Auth\LoginController@logout'
            ]);

            Route::get('/', function () {
                return redirect()->route('admin.dashboard');
            });

            Route::get('/dashboard', [
                'as' => 'admin.dashboard',
                'uses' => 'DashboardController@dashboard'
            ]);
        });

Route::middleware(['web', 'admin_guest'])
        ->prefix('admin')
        ->namespace('Wizinteractive\\Cms\\Http\\Controllers')
        ->group(function () {

            Route::get('login', [
                'as' => 'admin.login',
                'uses' => 'Auth\LoginController@showLoginForm'
            ]);

            Route::post('login', [
                'as' => 'admin.login',
                'uses' => 'Auth\LoginController@login'
            ]);

			if (config('cms.enable_register_link') == true){
			
				// Registration Routes...
            	Route::get('register', [
                	'as' => 'admin.register',
                	'uses' => 'Auth\RegisterController@showRegistrationForm'
            	]);

            	Route::post('register', [
                	'as' => 'admin.register',
                	'uses' => 'Auth\RegisterController@register'
            	]);
			}			
            

            // Password Reset Routes...
            Route::get('password/reset', [
                'as' => 'admin.password.request',
                'uses' => '\Wizinteractive\Cms\Http\Controllers\Auth\ForgotPasswordController@showLinkRequestForm'
            ]);

            Route::get('password/reset/{token}', [
                'as' => 'admin.password.reset',
                'uses' => 'Auth\ResetPasswordController@showResetForm'
            ]);

            Route::post('password/email', [
                'as' => 'admin.password.email',
                'uses' => 'Auth\ForgotPasswordController@sendResetLinkEmail'
            ]);

            Route::post('password/reset', [
                'as' => 'admin.password.post-reset',
                'uses' => 'Auth\ResetPasswordController@reset'
            ]);
        });
