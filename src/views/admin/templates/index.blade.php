@extends($view->resolve('admin.partials.master'))

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>{{ trans('resources.'.str_singular($resource).'.resource-name') }} list</h4>
                        @if (array_key_exists('create', $actions))
                        <a href="{{ route("admin.{$resource}.create") }}">
                            <button class="btn btn-default">
                                Adicionar item
                            </button>
                        </a>
                        @endif
                        @if (array_key_exists('update-list', $actions))
                        <a href="{{ route("admin.{$resource}.update-list") }}">
                            <button class="btn btn-default">
                                Actualizar
                            </button>
                        </a>
                        @endif
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (isset($errors) && !$errors->isEmpty())
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-warning">
                                            <strong>Warning!</strong> {{ $error }}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @if (Session::has('response'))
                                    <div class="alert alert-success">
                                        <strong>Success!</strong> {{ Session::get('response.message') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        {{-- @if (count($items))
                        <div class="row">
                            <div class="col-xs-12">
                                @foreach ($items->first()->getDisplay() as $column)
                                <span>{{ trans('resources.'.str_singular($resource).'.'.$column) }}</span>
                                @endforeach
                            </div>
                        </div>
                        <list
                            :items="{{ $items }}"
                            :resource="'{{ $resource }}'"
                            :token="'{{ $token }}'"
                        />
                        @else
                            <li class="list-group-item justify-content-between">
                            There are no {{ $resource }}
                            </li>
                        @endif --}}

                        @section('list-actions')
                        <div class="row">
                            <div class="col-xs-12">
                                <a href="#filters" data-toggle="collapse">
                                    <i class="material-icons">filter_list</i>
                                </a>
                                <a href="#order" data-toggle="collapse">
                                    <i class="material-icons">sort</i>
                                </a>
                            </div>
                        </div>

                        {{-- <panel-filters></panel-filters> --}}
                        <div id="filters" class="row collapse">
                            <div class="col-xs-12">
                                @section('filters')
                                    <h1>Filtros</h1>
                                @show
                            </div>
                        </div>

                        <div id="order" class="row collapse">
                            <div class="col-xs-12">
                                <h1>Ordenação</h1>
                            </div>
                        </div>
                        @show

                        @if (count($items))
                        <div class="row">
                            <div class="col-xs-12">
                                <table class="resource-index table">
                                    <thead>
                                        <tr>
                                            @foreach ($items->first()->getDisplay() as $column)
                                            <th>
                                                <span>{{ trans('resources.'.str_singular($resource).'.'.$column) }}</span>
                                            </th>
                                            @endforeach
                                            <th>
                                                <span class="pull-right">Actions</span>
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($items as $item)
                                        <tr>
                                            @foreach ($item->getDisplay() as $column)
                                            <td>
                                                <span>{{ $item->getColumnValue($column) }}</span>
                                            </td>
                                            @endforeach
                                            <td>
                                                <div class="actions centered">
                                                    @if (array_key_exists('update', $actions))
                                                    <span class="pull-right">
                                                        <a href="{{ route("admin.{$resource}.edit", ['id' => $item->id]) }}">
                                                        <i class="material-icons">mode_edit</i>
                                                        </a>
                                                    </span>
                                                    @endif
                                                    @if (array_key_exists('read', $actions))
                                                    <span class="pull-right">
                                                        <a href="{{ route("admin.{$resource}.show", ['id' => $item->id]) }}">
                                                        <i class="material-icons">chevron_right</i>
                                                        </a>
                                                    </span>
                                                    @endif
                                                    @if (array_key_exists('delete', $actions))
                                                    <span class="pull-right">
                                                        <form id="delete-form-{{ $item->id }}" action="{{ route('admin.'.$resource.'.destroy', ['id' => $item->id]) }}" method="POST">
                                                        {{ csrf_field() }}
                                                        <input type="hidden" name="_method" value="DELETE">
                                                        <a onclick="document.getElementById('delete-form-{{ $item->id }}').submit();"><i class="material-icons">delete_forever</i></a>
                                                        </form>
                                                    </span>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        {{ $items->links() }}
                        @else
                            There are no {{ $resource }}
                        @endif

                        {{-- <div class="row">
                            <div class="col-xs-12">
                                <ul class="list-group">
                                    @if (count($items))
                                    <li class="list-group-item justify-content-between">
                                         <div class="row">
                                            <div class="col-xs-9">
                                                <table>
                                                    <tr>

                                                        @foreach ($items->first()->getDisplay() as $column)
                                                        <td>
                                                            <span>{{ trans('resources.'.str_singular($resource).'.'.$column) }}</span>
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </li>
                                    @foreach($items as $item)
                                    <li class="list-group-item justify-content-between">
                                        <div class="row">
                                            <div class="col-xs-9">
                                                <table>
                                                    <tr>
                                                        @foreach ($item->getDisplay() as $column)
                                                        <td>
                                                            <span>{{ $item->getColumnValue($column) }}</span>
                                                        </td>
                                                        @endforeach
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="col-xs-3">
                                                @if (in_array('update', $actions))
                                                <span class="pull-right">
                                                    <a href="{{ route("admin.{$resource}.edit", ['id' => $item->id]) }}">
                                                    <i class="material-icons">mode_edit</i>
                                                    </a>
                                                </span>
                                                @endif
                                                @if (in_array('read', $actions))
                                                <span class="pull-right">
                                                    <a href="{{ route("admin.{$resource}.show", ['id' => $item->id]) }}">
                                                    <i class="material-icons">chevron_right</i>
                                                    </a>
                                                </span>
                                                @endif
                                                @if (in_array('delete', $actions))
                                                <span class="pull-right">
                                                    <form id="delete-form-{{ $item->id }}" action="{{ route('admin.'.$resource.'.destroy', ['id' => $item->id]) }}" method="POST">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    <a onclick="document.getElementById('delete-form-{{ $item->id }}').submit();"><i class="material-icons">delete_forever</i></a>
                                                    </form>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                    </li>
                                    @endforeach
                                    @else
                                        <li class="list-group-item justify-content-between">
                                        There are no {{ $resource }}
                                        </li>
                                    @endif
                                </ul>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection