@extends($view->resolve('admin.partials.master'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading"><h1>{{ $model->designation }}</h1></div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (isset($errors) && !$errors->isEmpty())
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-warning">
                                            <strong>Warning!</strong> {{ $error }}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @if (Session::has('response'))
                                    <div class="alert alert-success">
                                        <strong>Success!</strong> {{ Session::get('response.message') }}
                                    </div>
                                @endif
                            </div>
                        </div>

                        @foreach($model->getColumns() as $column => $type)
                        <div class="row">
                            <div class="col-xs-12">
                                <span><strong>{{ $model->getColumnLabel($column) }}:</strong></span>
                                <span>{{ $model->$column }}</span>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection