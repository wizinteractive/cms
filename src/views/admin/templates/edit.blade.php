@extends($view->resolve('admin.partials.master'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">

                    <div class="panel-heading">
                        @if ($panel === 'update')
                        <h1>Edit {{ trans('resources.'.str_singular($resource).'.resource-name') }} - {{ $model->designation }}</h1>
                        @else
                        <h1>Create {{ trans('resources.'.str_singular($resource).'.resource-name') }} </h1>
                        @endif
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (isset($errors) && !$errors->isEmpty())
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-warning">
                                            <strong>Warning!</strong> {{ $error }}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @if (Session::has('response'))
                                    <div class="alert alert-success">
                                        <strong>Success!</strong> {{ Session::get('response.message') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <form action="{{ route('admin.'.$resource.'.'.$panel, ['id' => $model->id]) }}" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xs-8">
                                    {{ csrf_field() }}
                                    @if (!is_null($model->id))
                                         <input type="hidden" name="_method" value="PUT">
                                    @endif

                                    @foreach($model->getColumns() as $column => $type)
                                        @component("cms::admin.partials.components.input-{$type}", [
                                            'model' => $model,
                                            'column' => $column,
                                            'label' => $model->getColumnLabel($column),
                                            'value' => is_null(old($column)) ? $model->$column : old($column)
                                        ])
                                        @endcomponent
                                    @endforeach

                                    @if($model->hasDirectRelations())
                                        @foreach($directRelationsLists as $dr => $list)
                                            @component("cms::admin.partials.components.input-relation", [
                                                'column' => $model->getDirectRelationColumn($dr),
                                                'label' => $model->getDirectRelationLabel($dr),
                                                'value' => is_null(old($model->getDirectRelationColumn($dr))) ? $model->getDirectRelationId($dr) : old($model->getDirectRelationColumn($dr)),
                                                'options' => $list
                                            ])
                                            @endcomponent
                                        @endforeach
                                    @endif

                                    @if($model->hasIndirectRelations())
                                        @foreach($indirectRelationsLists as $dr => $list)
                                            @component("cms::admin.partials.components.input-indirect-relation", [
                                                'relation' => $model->getIndirectRelation($dr),
                                                'label' => $model->getIndirectRelationLabel($dr),
                                                'value' => is_null(old($model->getIndirectRelation($dr))) ? $model->getIndirectlyRelated($dr) : old($model->getIndirectRelation($dr)),
                                                'options' => $list
                                            ])
                                            @endcomponent
                                        @endforeach
                                    @endif

                                </div>
                                <div class="col-xs-4">
                                    @if ($model->isMediable())
                                        @foreach($model->mediaSlots() as $mediaSlot)
                                            @component("cms::admin.partials.components.media-box", [
                                                'mediaSlot' => collect($mediaSlot),
                                                'media' => $model->getMediaFromSlot($mediaSlot['name'])
                                            ])
                                            @endcomponent
                                        @endforeach
                                    @endif

                                    @if ($model->exists && $model->isTranslatable())
                                    <translation-box
                                        :model="{{ $model }}"
                                        :translations="{{ $model->translations }}"
                                    />
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <button type="submit" class="btn btn-default">Salvar</button>
                                </div>
                            </div>
                        </form>

                        @if (array_key_exists('delete', $actions) && $panel === 'update')
                        <form action="{{ route('admin.'.$resource.'.destroy', ['id' => $model->id]) }}" method="POST">
                            {{ csrf_field() }}
                            @if (!is_null($model->id))
                                 <input type="hidden" name="_method" value="DELETE">
                            @endif
                            <button type="submit" class="btn btn-default">Apagar</button>
                        </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection