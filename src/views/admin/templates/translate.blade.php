@extends($view->resolve('admin.partials.master'))

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1>Translate {{ trans('resources.'.str_singular($resource).'.resource-name') }} - {{ $model->designation }} to {{ $model->supported_translation_routes[$lang]['name'] }}</h1>
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                @if (isset($errors) && !$errors->isEmpty())
                                    @foreach($errors->all() as $error)
                                        <div class="alert alert-warning">
                                            <strong>Warning!</strong> {{ $error }}
                                        </div>
                                    @endforeach
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                @if (Session::has('response'))
                                    <div class="alert alert-success">
                                        <strong>Success!</strong> {{ Session::get('response.message') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        <form action="{{ route('admin.'.$resource.'.post-translate', ['id' => is_null($model->translates_id) ? $model->id : $model->translates_id, 'lang' => $lang]) }}" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-xs-8">
                                    {{ csrf_field() }}
                                    @foreach($model->getTranslatableColumns() as $column => $type)
                                        @component("cms::admin.partials.components.input-{$type}", [
                                            'model' => $model,
                                            'column' => $column,
                                            'label' => $model->getColumnLabel($column),
                                            'value' => $model->$column
                                        ])
                                        @endcomponent
                                    @endforeach
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-4">
                                    <button type="submit" class="btn btn-default">Salvar</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection