@extends('cms::admin.partials.master')

@section('content')
  <main>
        <div class="forgot-main">
            <div class="header-gradient"></div>
            <div class="logo">
                <img src="{{ url('/') }}/images/header/logo.png">
            </div>
            <div class="forgot-content">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="rightside floating">
                    <div class="login-wrapper">
                        <h3>Esqueci-me da password</h3>
                        <span class="form-intro">Introduza o e-mail de acesso à sua conta</span>
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" type="email" class="form-control" placehodler="e-mail" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary link-button">
                                        Recuperar password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="back-link">
                    <a href="{{ url('/') }}">< Voltar</a>
                </div>
            </div>
        </div>
    </main>
@endsection
