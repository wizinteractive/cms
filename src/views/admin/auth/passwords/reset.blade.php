@extends('cms::admin.partials.master')

@section('content')
   <main>
        <div class="header-gradient"></div>
        <div class="reset-main">    
            <div class="logo">
                <img src="{{ url('/') }}/images/header/logo.png">
            </div>
            <div class="reset-content container">
                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="rightside floating">
                    <div class="login-wrapper">
                        <h3>Alterar Password</h3>
                        <span class="form-intro">Redefina a password de acesso à sua conta</span>
                        <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.password.request') }}">
                            <input type="hidden" name="token" value="{{ $token }}">
                            {{ csrf_field() }}
                            <label>Email</label>
                           <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input id="email" placehodler="e-mail" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        <label>Nova password</label>
                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input id="password" placehodler="password" type="password" class="form-control" name="password" required>

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                        <label>Repetir nova password</label>
                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                            <input id="password-confirm" placehodler="repetir password" type="password" class="form-control" name="password_confirmation" required>

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>

                            <div class="form-group">
                                <div>
                                    <button type="submit" class="btn btn-primary link-button">
                                        Redefinir password
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
