<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- Styles -->
    @section('styles')
        <link href="{{ asset('vendor/css/admin.css') }}" rel="stylesheet">
    @show
</head>
<body>
    <div id="admin-app">
        @include($view->resolve('admin.partials.navbar'))

        @yield('content')
    </div>

    @section('pre-scripts')
    @show

    @section('tiny-mce')
    @show

    @section('scripts')
        <script src="{{ asset('vendor/js/admin.js') }}"></script>
    @show
</body>
</html>
