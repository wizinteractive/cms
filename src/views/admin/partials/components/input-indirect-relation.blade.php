<div class="form-group form-select">
    <label for="{{ $relation }}[]" class="form-select-label">
            {{ $label }}
    </label>
    <select class="form-control" name="{{ $relation }}[]" multiple="">
        @foreach($options as $option)
        <option {{ in_array($option->id, $value) ? 'selected' : '' }} value="{{ $option->id }}">
            {{ $option->designation }}
        </option>
        @endforeach
    </select>
</div>