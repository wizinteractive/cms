<div class="form-group">
    <label for="{{ $column }}">{{ $label }}</label>
    <input type="number" min="0" class="form-control" name="{{ $column }}" placeholder="" value="{{ $value }}">
</div>