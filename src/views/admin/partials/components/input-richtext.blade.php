<div class="form-group">
    <label for="{{ $column }}">{{ $label }}</label>
    <textarea class="form-control richtext" name="{{ $column }}" rows="3">{{ $value }}</textarea>
</div>

@section('tiny-mce')
    <script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey={{ env('TINY_MCE_KEY', '') }}"></script>
    <script>tinymce.init({ selector:'.richtext' });</script>
@endsection