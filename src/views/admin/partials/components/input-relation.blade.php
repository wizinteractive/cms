<div class="form-group form-select">
    <label for="{{ $column }}" class="form-select-label">
            {{ $label }}
    </label>
    <select class="form-control" name="{{ $column }}">
        <option selected disabled value="null">Choose...</option>
        @foreach($options as $option)
        <option {{ $option->id == $value ? 'selected' : '' }} value="{{ $option->id }}">
            {{ $option->designation }}
        </option>
        @endforeach
    </select>
</div>