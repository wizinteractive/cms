<div class="form-group">
    <label for="{{ $column }}">{{ $label }}</label>
    <input type="text" class="form-control" name="{{ $column }}" placeholder="" value="{{ $value }}">
</div>