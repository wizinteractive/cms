<div class="form-group form-select">
    <label for="{{ $column }}" class="form-select-label">
            {{ $label }}
    </label>
    <select class="form-control" name="{{ $column }}">
        <option selected disabled value="null">Choose...</option>
        @foreach($model->getFieldOptions($column) as $optionValue => $optionName)
        <option {{ $optionValue == $value ? 'selected' : '' }} value="{{ $optionValue }}">
            {{ $optionName }}
        </option>
        @endforeach
    </select>
</div>