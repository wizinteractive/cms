<div class="media-box">
    <div class="row insert">
        <div class="col-xs-12">
            <label for="{{ $mediaSlot['name'].'[]' }}">{{ $mediaSlot['label'] }}</label>
            <input class="" type="file" name="{{ $mediaSlot['name'].'[]' }}" id="file" {{ $mediaSlot['multiple'] ? 'multiple' : '' }}/>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <ul class="list-unstyled">
                @foreach($media as $file)
                {{-- <li class="media">
                    <img class="d-flex mr-3" src="{{ '/' . $file->path }}" alt="Generic placeholder image">
                    <div class="media-body">
                    <h5 class="mt-0 mb-1">{{ $file->mime_type }}</h5>
                    </div>
                </li> --}}
                <media-file :media="{{ $file }}"></media-file>
                @endforeach
            </ul>
        </div>
    </div>
</div>