<?php

namespace Wizinteractive\Cms\Support\Views;

class ViewResolver
{
    public function resolve($view)
    {
        if (file_exists(app()->resourcePath().'/views/'.str_replace('.', '/', $view).'.blade.php')) {
            return $view;
        }
        return "cms::{$view}";
    }
}
