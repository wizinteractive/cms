<?php

namespace Wizinteractive\Cms\Support;

use Wizinteractive\Cms\Contracts\AdminServiceContract;
use Wizinteractive\Cms\Support\AdminResourceViewResolver;

class AdminResourceManager implements AdminServiceContract
{
    protected $service;

    public function __construct(AdminServiceContract $service)
    {
        $this->service = $service;
        $this->viewResolver = new AdminResourceViewResolver;
    }

    public function resolveResource($path)
    {
        return $this->service->resolveResource($path);
    }

    public function resourceExists($resource)
    {
        return $this->service->resourceExists($resource);
    }

    public function instantiateResource($resource, $directory = null)
    {
        return $this->service->instantiateResource($resource);
    }

    public function getResourceClass($resource, $directory = null)
    {
        return $this->service->getResourceClass($resource, $directory);
    }

    public function getResourceActions($resource, $directory = null)
    {
        return $this->service->getResourceActions($resource, $directory);
    }

    public function getResourceDirectRelations($resource, $directory = null)
    {
        return $this->service->getResourceDirectRelations($resource, $directory);
    }

    public function getResourceDirectRelationsLists($resource, $directory = null)
    {
        return $this->service->getResourceDirectRelationsLists($resource, $directory);
    }

    public function getResourceIndirectRelations($resource, $directory = null)
    {
        return $this->service->getResourceIndirectRelations($resource, $directory);
    }

    public function getResourceIndirectRelationsLists($resource, $directory = null)
    {
        return $this->service->getResourceIndirectRelationsLists($resource, $directory);
    }

    public function resolveResourceView($resource, $action)
    {
        return $this->viewResolver->resolveView($resource, $action);
    }
}
