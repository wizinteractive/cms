<?php

namespace Wizinteractive\Cms\Services;

use Wizinteractive\Cms\Contracts\AdminServiceContract;

class AdminService implements AdminServiceContract
{
    protected $defaultResourceDirectory = 'App\\Models\\';

    /**
     * Resolve the name of an admin resource
     *
     * @param  string $path
     * @return string
     */
    public function resolveResource($path)
    {
        $explodedPath = explode('/', $path);

        if ($explodedPath[0] !== 'admin' || !isset($explodedPath[1])) {
            return null;
        }

        return $explodedPath[1];
    }

    /**
     * Checks if a given resource exists
     *
     * @param  string $resource
     * @return bool
     */
    public function resourceExists($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        $className = $directory . studly_case(str_singular($resource));

        return class_exists($className);
    }

    /**
     * Creates an instance of a given admin resource
     *
     * @param  string $resource
     * @return Model $resourceModel or null
     */
    public function instantiateResource($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        if ($this->resourceExists($resource, $directory)) {
            $className = $directory . studly_case(str_singular($resource));

            $resourceModel = new $className;

            return $resourceModel;
        }

        return null;
    }

    /**
     * Gets the class of a given admin resource
     *
     * @param  string $resource
     * @return string $className or null
     */
    public function getResourceClass($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        if ($this->resourceExists($resource, $directory)) {
            $className = $directory . studly_case(str_singular($resource));

            return $className;
        }

        return null;
    }

    /**
     * Gets the set of actions that can be applied to a given admin resource
     *
     * @param  string $resource
     * @return array $actions or null
     */
    public function getResourceActions($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        if ($this->resourceExists($resource, $directory)) {
            $className = $directory . studly_case(str_singular($resource));

            return (new $className)->getActions();
        }

        return null;
    }

    /**
     * Gets the direct relations classes of a given resource
     *
     * @param  string $resource
     * @return array $directRelations
     */
    public function getResourceDirectRelations($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        $resourceModel = $this->instantiateResource($resource, $directory);

        $directRelations = [];

        if ($resourceModel->hasDirectRelations()) {
            foreach ($resourceModel->getDirectRelations() as $relationResource => $relationInfo) {
                if (is_array($relationInfo) && array_key_exists('constraints', $relationInfo)) {
                    $directRelations[$relationResource] = [
                                                            'class' => $this->getResourceClass($relationResource, $directory),
                                                            'constraints' => $relationInfo['constraints']
                                                        ];
                    continue;
                }
                $directRelations[$relationResource] = $this->getResourceClass($relationResource, $directory);
            }
        }

        return $directRelations;
    }

    /**
     * Gets the direct relations lists of a given resource
     *
     * @param  string $resource
     * @return array lists
     */
    public function getResourceDirectRelationsLists($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        $resourceModel = $this->instantiateResource($resource, $directory);

        if ($resourceModel->hasDirectRelations()) {
            $relationsClasses = $this->getResourceDirectRelations($resource, $directory);

            $lists = [];

            foreach ($relationsClasses as $resource => $className) {
                if (is_array($className)) {
                    $lists[$resource] = (new $className['class'])->isTranslatable() ? $className['class']::where('lang', \App::getLocale()) : $className['class']::query();

                    if (array_key_exists('constraints', $className)) {
                        foreach ($className['constraints'] as $key => $value) {
                            $lists[$resource] = $lists[$resource]->where($key, $value);
                        }
                    }

                    $lists[$resource] = $lists[$resource]->get();
                    continue;
                }
                $lists[$resource] = (new $className)->isTranslatable() ? $className::where('lang', \App::getLocale())->get() : $className::get();
            }
            return $lists;
        }

        return null;
    }

    /**
     * Gets the direct relations classes of a given resource
     *
     * @param  string $resource
     * @return array $directRelations
     */
    public function getResourceIndirectRelations($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        $resourceModel = $this->instantiateResource($resource, $directory);

        $indirectRelations = [];
        if ($resourceModel->hasIndirectRelations()) {
            foreach ($resourceModel->getIndirectRelations() as $relationResource => $relationInfo) {
                if (is_array($relationInfo) && array_key_exists('constraints', $relationInfo)) {
                    $indirectRelations[$relationResource] = [
                                                            'class' => $this->getResourceClass($relationResource, $directory),
                                                            'constraints' => $relationInfo['constraints']
                                                        ];
                    continue;
                }
                $indirectRelations[$relationResource] = $this->getResourceClass($relationResource, $directory);
            }
        }
        return $indirectRelations;
    }

    /**
     * Gets the direct relations lists of a given resource
     *
     * @param  string $resource
     * @return array lists
     */
    public function getResourceIndirectRelationsLists($resource, $directory = null)
    {
        if (is_null($directory)) {
            $directory = $this->defaultResourceDirectory;
        }

        $resourceModel = $this->instantiateResource($resource, $directory);

        if ($resourceModel->hasIndirectRelations()) {
            $relationsClasses = $this->getResourceIndirectRelations($resource, $directory);
            $lists = [];

            foreach ($relationsClasses as $resource => $className) {
                if (is_array($className)) {
                    $lists[$resource] = (new $className['class'])->isTranslatable() ? $className['class']::where('lang', \App::getLocale()) : $className['class']::query();

                    if (array_key_exists('constraints', $className)) {
                        foreach ($className['constraints'] as $key => $value) {
                            $lists[$resource] = $lists[$resource]->where($key, $value);
                        }
                    }

                    $lists[$resource] = $lists[$resource]->get();
                    continue;
                }
                $lists[$resource] = (new $className)->isTranslatable() ? $className::where('lang', \App::getLocale())->get() : $className::get();
            }
            return $lists;
        }

        return null;
    }
}
