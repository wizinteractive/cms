<?php

namespace Wizinteractive\Cms\Contracts;

interface AdminServiceContract
{
    /**
     * Resolve the name of an admin resource
     *
     * @param  string $path
     * @return string
     */
    public function resolveResource($path);

    /**
     * Checks if a given resource exists
     *
     * @param  string $resource
     * @return bool
     */
    public function resourceExists($resource);

    /**
     * Creates an instance of a given admin resource
     *
     * @param  string $resource
     * @return Model $resourceModel or null
     */
    public function instantiateResource($resource, $directory = null);

    /**
     * Gets the class of a given admin resource
     *
     * @param  string $resource
     * @return string $className or null
     */
    public function getResourceClass($resource, $directory = null);

    /**
     * Gets the set of actions that can be applied to a given admin resource
     *
     * @param  string $resource
     * @return array $actions or null
     */
    public function getResourceActions($resource, $directory = null);

    /**
     * Gets the direct relations classes of a given resource
     *
     * @param  string $resource
     * @return array $directRelations
     */
    public function getResourceDirectRelations($resource, $directory = null);

    /**
     * Gets the direct relations lists of a given resource
     *
     * @param  string $resource
     * @return array $lists
     */
    public function getResourceDirectRelationsLists($resource, $directory = null);
}
