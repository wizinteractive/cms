<?php

namespace Wizinteractive\Cms;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;
use Mcamara\LaravelLocalization\LaravelLocalizationServiceProvider;
use Wizinteractive\Cms\Services\AdminService;
use Wizinteractive\Cms\Contracts\AdminServiceContract;

class CmsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // resolve admin service to be used
        $this->app->bind(AdminServiceContract::class, function ($app) {
            $service = studly_case(env('APP_ADMIN_SERVICE', 'admin_service'));
            return $app->make("Wizinteractive\Cms\Services\\${service}");
        });

        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/views', 'cms');

        $this->publishes([
            __DIR__.'/../public/js/' => public_path('vendor/js'),
            __DIR__.'/../public/css/' => public_path('vendor/css'),
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/cms.php', 'cms');
        $this->mergeConfigFrom(__DIR__.'/config/laravellocalization.php', 'laravellocalization');

        $this->app['router']->aliasMiddleware('admin', \Wizinteractive\Cms\Http\Middleware\Admin::class);
        $this->app['router']->aliasMiddleware('admin_guest', \Wizinteractive\Cms\Http\Middleware\RedirectIfAuthenticated::class);

        $this->app->register(LaravelLocalizationServiceProvider::class);
        $this->app->booting(function () {
            $loader = AliasLoader::getInstance();
            $loader->alias('LaravelLocalization', \Mcamara\LaravelLocalization\Facades\LaravelLocalization::class);
        });
    }
}
