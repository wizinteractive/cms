## Wizinteractive boilerplate

# Installation notes

After running `composer install`:

- Add the following to your `config\auth.php` in the `guards` key

```
'admin' => [
    'driver' => 'session',
    'provider' => 'admin_users',
],
```

This will set the guard that should be used to authenticate the users on the backoffice.

- Add, to the same file, in the `providers` array:

```
users' => [
    'driver' => 'eloquent',
    'model' => App\Models\User::class,
],

'admin_users' => [
    'driver' => 'eloquent',
    'model' => Wizinteractive\Cms\Models\AdminUser::class,
],
```

- Add, to the same file in the `passwords` array:

```
    'users' => [
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
    ],
    'admin_users' => [
        'provider' => 'admin_users',
        'table' => 'admin_password_resets',
        'expire' => 60,
    ]
```

This will define the tables that should be used to password resets.

Finally run `php artisan vendor:publish --tag=public --force` in the command line in the root of the project.

# Adding resources

Create the resource migration via cli with `php artisan make:migration create_table_name_table`.

If the model is to be translatable it should have a `translates_id` and `lang` column and translation routes should be defined to at `routes\admin.php`. An example follows the end of this section.

Model columns designations should be set at `resources/lang/{locale}/resources.php` as in the example array in that file.

Run the migrations.

Add the model class to `app\Models` and make it extend the class `Wizinteractive\Cms\Models\BaseModel`;

Add the model controller in `app\Http\Controller\Admin\ControllerName.php` and make it extend the `Wiz\Cms\Http\Controllers\AdminController.php`

Add the resource route to the `routes\admin.php` like:

```
    Route::get('resource/translate/{id}/{lang}', [
        'as' => 'admin.resource.translate',
        'uses' => 'ResourceController@translate'
    ]);

    Route::post('resource/translate/{id}/{lang}', [
        'as' => 'admin.resource.post-translate',
        'uses' => 'ResourceController@postTranslate'
    ]);

    Route::resource('resource', 'ResourceController', [
        'as' => 'admin'
    ]);
```



# Model properties

- `resource` => name that will be used by the admin controller to resolve the resource
- `designationField` => column of the resource's table that will be used to designate the resource
- `directRelations` => array containing directly related resources. Each array entry consists of the related resource name(key) and the column to the foreign key in the resource's table (value) by default. If the value is an array, additional information can be provided to establish the relationship. Further information bellow.
- `indirectRelations` => array containing indirectly related resources. Each array entry consists of the related resource name(key) and the relationship name (defined on the model) by defaul. If the value is an array, additional information can be provided to establish the relationship. Further information bellow.
- `columns` => columns that will be editable in the create/edit panel, the value of each column corresponds to its type and will define the type of input field
- `display` => columns that will be displayed when listing the resource
- `fillable` => columns that can be created/updated at once when passing an array to the Model
- `rules` => fields that should be validated when creating/updating a resource
- `translatable` => boolean that defines if this model can be translated. Translatable models should have a `translates_id` and `lang` column on their table
- `translatableColumn` => 'defines the fields that can be translated'

# Relations

As referred before models can have direct or indirect relations with another models. These relations can be defined in their simplest forms as specified above, but there are cases where we might need additional configuration.

- `direct relation` can receive as its value on array containing 3 key/value pairs. `column` defines the column of the table that defines the relationship, `rule` defines the rule that should be used on validation when saving this resource and finally `constraints` is an array where one can define certains how the resources should be related. For example, one can define `active => 1` in the constraints array and only the active resources while be fetched in the query.

- `indirect relation` functions exactly like the direct relations but instead of defining the `column` one should define the name of the method that establishes the relation.

# Fields caveats

Almost all fields are generate simply by defining its type on the `$columns` property of the model. The exception being the type `select`, with this type we will need to define an additional method on the model to resolve the options that will be available. The method for the field `type` should be `getFieldTypeOptions` and must return an associative array with the value/labels pairs.

# Overriding Route Controllers

If for any reason you need to extend or redo the controllerthat is being used to resolve a certain route you have two options:

- Add to your RouteServiceProvider `boot` method the following:

```
// example concerning the login route

Route::get('admin/login')->uses('App\Http\Controllers\Admin\Auth\LoginController@showLoginForm');
```
- The second option is to define it directly in the application's route files like you would do to any route





